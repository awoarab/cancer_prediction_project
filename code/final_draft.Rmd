---
title: "Machine-Learning Driven Classification of Tumor Types Using Transcriptomics Data"
author: "Group 7 - BIN315"
date: '2022-11-28'
output: 
  rmdformats::html_clean:
      df_print: paged
      code_folding: hide
      toc: true
      toc_depth: 2
      toc_float: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE, error = FALSE)
```

# Abstract

There has in recent times been a focus on utilizing different omics data to characterize tumors and cancer pathology. Investigating the transcriptomic differences between cancer types if of great interest since it may help identify potential pathways relevant for the development of treatment. Furthermore, transcriptomics analysis may aid in the accurate diagnosis of cancer subtypes. Although differential expression analysis (DEA) allows for investigation of interesting genes, such analysis cannot predict disease status from transcriptomics data. We therefore proposed using machine learning methods to differentiate between tumor samples. We showed how simpler supervised machine learning methods, like $k$-nearest neighbors, and more complex ones, like random forest, accurately classify tumor samples with an accuracy over 90%. The broader implications of these results is that such machine learning methods may be used in classifying tumor sub types (eg. sub types of breast cancer). Such methods may therefore be useful use in diagnosis of patients with complex, heterogeneous cancer types (like breast cancer). (1)

# Introduction

Cancer is a disease in which the cells start to divide abnormally (2). Tissue created by these cells accumulates into tumors, which may be benign or malignant. In the last case, the tumor will cause pain, loss of function and eventually death as it spreads and invade other tissues of the body (metastasis). It is one of the leading causes of death worldwide, next to heart disease (3)

Genes that are associated with cancer are already well characterized. Such genes include BRCA in hereditary breast cancer (4), CDH1, MLH1 and MSH2 in gastrointestinal cancer (5),  and CDKN2A in skin cancer (6). 

Using different omics data to characterize cancer types and their pathology has been gaining increasing attention (7-8). There are several initiatives to collect and publicize omics data from cancer patients. Among these initiatives is The Cancer Genome Atlas (TCGA) Research Network. This is an extensive project that aims to understand several cancer types on a molecular level by analyzing human tumors on genomic, transcriptomic, metabolomic and strucutural level(9). In other word it is a collection of multi-omics data on various cancer types. The data can be found in the data repository of The National Cancer Institute (USA).

In this project, we are using RNA sequence (RNA-Seq) data from the TCGA Research Network (specifically, the TCGA-project). The objectives of this project are:

-   Evaluate how well machine learning methods predict tumor types from transcriptomics data

-   Evaluate which features the machine learning methods deem important to separate cancer types

We will not classify all tumor types in the TCGA-project, rather, we will select tumor types that are difficult to separate.

Following a supervised learning pipeline, we will apply machine learning methods to the data set:

![Figure 1: Illustration of pipeline analysis](https://media.giphy.com/media/kRpsMzUkGzKim03oqO/giphy.gif)

# Methods

## Preprocessing with DESeq2:

`DESeq2` is an R-package from the BioConductor repository used for differential gene expression analysis (10). The package contains several functions that can be used on read-count data to normalize and variance stabilize the data. Normalizing RNA-Seq data in `DESEq2` accounts for realities such as sequencing depth, technical biases, and gene length. In other words, we can use the package to normalize for differences in library size between samples.\
The functions `EstimateSizeFactor` and `VarianceStabilizingTransformation` (VST) for variable distribution of expression. VST ensures that the variance of expression of genes are independent from their mean expression.

## Principal Component Analysis

Principal Component Analysis (PCA) is a dimension reduction method (11). In this paper, PCA is used to visualize high dimensional data in two dimensions. The idea of PCA is to find orthogonal directions in the space spanned by predictors (feature space). The directions are selected so that they explain the most variance in the predictor matrix. The orthogonal directions are referred to as principal components (PCs). Each PC will thus have a standard deviation associated with itself, which is simply the square root of variance it explains. 

## Machine Learning Methods

We will be using functions from the `caret`-package (12). This is because functions in this package allow for parallelization ( which enables efficient calculations with limited computational resources). Code structure is inspired by the tutorial e-book (13).

### $k$-Nearest Neighbors Algorithm

$k$-nearest neighbors ($k$NN) algorithm is a supervised machine learning method that solves classification problems (14). The method classifies an unknown observation based on the most prevalent class among its $k$-nearest neighbors in the feature space. The parameter $k$ is set by the user, and it describes the number of neighboring observations to consider when classifying unknown observations. In this paper, we will perform leave-one-out cross validation (LOO-CV) to find the $k$-value that yields the highest accuracy.

### Random Forest

Random forest classification is also a supervised machine learning method that uses an ensemble of decision trees (15). Decision trees split the data and create nodes using features in a manner that yields the highest information gain for each node split (15). Random forest method obtains averages of decision trees for subsets of data in order to improve the accuracy of predictions. In this paper we chose to tune only one parameter, `mtry` , which is the number of variables sampled for each split. The importance of variables is based in  Gini impurity (16).  

Random forest assign each feature an importance, we can therefore extract the most important genes for classification for differentiation between tumor types. Using Random forest instead of differential analysis (DEA) is not unheard of. In the study by Wernic and Shemirani (17). Random forest and DEA was compared over TCGA-data sets. the principle for this random forest classification loosely based on the methods of this paper, although our goals are different.

### Performance Metric

The two machine learning methods' performance will be evaluated using the accuracy of their predictions on a testing set. For each method, we will create a confusion matrix to calculate both the accuracy of predictions and see which tumor types are classified.

# Analysis

## Pre-processing

In the following chunk, we load data, packages and preprocess the data set as described in the methods section.

```{r}
# Packages
library(DESeq2)
library(magrittr)
library(ggplot2)
library(caret)
library(doParallel)

# Pre-processing
load("data/TCGA_v2.RData")

# Normalizing 
dds <- round(expr) %>% DESeqDataSetFromMatrix(colData = DataFrame(classes),
                                              design = ~classes) 
# Variance stablising transformation (VST):
dds <- estimateSizeFactors(dds)
expr.norm <- varianceStabilizingTransformation(dds)
```

## Exploratory Analysis and Selecting Cancer Type

We may now look at the dispersion of samples in a space spanned by PCs. Following code chunk extracts PCs and plots score plot:

```{r, cache=TRUE, fig.cap= 'Figure 2: Score plot for the two first PCs'}
# PCA-analysis:
pca.model <- prcomp(t(assay(expr.norm)), center = TRUE, scale. = TRUE)
# Extracting score matrix and adding classes for plotting:
score.mat <- data.frame('PC1' = pca.model$x[,1],
                        'PC2' = pca.model$x[,2], 
                        'Cancer.Type' = classes)

# Extracting explained variance (of total) in %
explained.variance <- round(100*(pca.model$sdev^2)/sum(pca.model$sdev^2))

#Colors
cols <- as.vector(pals::glasbey(13))
ggplot(score.mat, aes(x = PC1, y = PC2, color = Cancer.Type )) +
  geom_point(size = 1, alpha = 0.6) +
  scale_color_manual(values = cols) +
  xlab(paste0("PC1 ", explained.variance[1], "%"))+
  ylab(paste0("PC2 ", explained.variance[2], "%")) +
  labs(title = "Score Plot for VST and Normalised Expression Data")

```

We see that the 5 cancer types that are difficult to seperate in the score plot is:

-   Pancreas
-   Lung
-   Breast
-   Rectum
-   Stomach

The code chunk below selects these cancer types.

```{r}
idx <- which(classes %in% c("Pancreas", "Lung", "Breast", "Rectum", "Stomach"))
expr.reduced <- assay(expr.norm)[,idx]
classes.redcued <- classes[idx] %>% droplevels()
```

## Transposition, Training and Testing Sets

We transpose the expression matrix, since  we are interested in separating the sample (hence samples should be rows. We also create separate training and testing sets for the predictor matrix (`X`) and class vector (`Y`):

```{r}
# Transposing
expr.reduced <- t(expr.reduced) 

# Testing and Training Sets
set.seed(1212)
n <- nrow(expr.reduced)
selected.rows <- split(sample(n), rep(c("train", "test"), length = n))
X.train <- expr.reduced[selected.rows$train, ]
Y.train <- classes.redcued[selected.rows$train]
X.test <- expr.reduced[selected.rows$test, ]
Y.test <- classes.redcued[selected.rows$test]
```

## $k$NN-Classification Training/Tuning:

In the following code chunk we run LOOCV for for $k \in [1,15]$. The results for accuracy is shown in the figure below. 
```{r, cahce = TRUE, fig.cap='Figure 3: Plot of accuracy results for knn using LOOCV'}
# Object used to define parameters for training/tuning
fitControl <- trainControl(method = "LOOCV", 
                           number = 1)
# Search grid for k
k.grid <- expand.grid("k" = seq(1,15))

df_train <- as.data.frame(X.train) %>% cbind(data.frame("Cancer.Type" = Y.train))

# Parallization
cl <- makeCluster(detectCores() - 2)    # Have 8 cores - 2 = 6
registerDoParallel(cl)
set.seed(1212)  

# Model tuning
knn.model <- train(Cancer.Type ~. , 
                   data = df_train, 
                   method = "knn", 
                   trControl = fitControl, 
                   tuneGrid = k.grid)
stopCluster(cl)


# Plotting results 
plot(x = knn.model$results$k,y = knn.model$results$Accuracy, type = "o", pch = 16, col = 1, ylab = "Accuracy", xlab = "k")
cat("Best accuracy for k is achieved by k =", knn.model$bestTune[[1]])
```

## $k$NN Predictions

In the code chunk above we compute the predictions using the fitted model (`knn.model`). We also make a confusion matrix:

```{r}
df_test <- as.data.frame(X.test) 
mixlm::confusion(true = Y.test, 
                 predicted = predict(knn.model, newdata = df_test))

```
Table 1: Prediction results for $k$NN

## Random Forest Training/Tuning

First we train the model. As suggested by the article, we being with an `mtry` in $[\sqrt{\text{no. of columns}}, 924]$. Results for this is shown in the table below. 

```{r, cache=TRUE}

# Object used for training/tuning of model
fitControl <- trainControl(method = "cv", 
                           n = 10)
# Grid search for mtry
mtry.grid <- expand.grid("mtry" = c(seq(from = round(sqrt(15420)), to = 1000, by = 200)))

# Convert gene names to remove |-sign (avoid error)
names(df_train) <- make.names(names(df_train))

# Detecting clusters and preparing parallelization
cl <- makeCluster(detectCores() - 1)    # Have 8 cores - 1 = 7
registerDoParallel(cl)

set.seed(1212)
#Fitting training models
rf.model <- train(Cancer.Type ~. , 
                   data = df_train, 
                   method = "rf", 
                   trControl = fitControl, 
                   tuneGrid = mtry.grid )
stopCluster(cl)
# Returning accurcy of models
rf.model
```
Table 2: Random forest accuracy results using (CV, folds = 10)

## Random Forest Predictions and Variable Importance

First we make predictions for the testing set:

```{r}
# Renaming columns
names(df_test) <- make.names(names(df_test))

# Predictions
mixlm::confusion(true = Y.test, predicted = predict(rf.model, newdata = df_test))

```
Table 3: Prediction results for random forest


Secondly, we extract the 200 most important features (genes) and collect these in a *.txt*-file. The results from Enrichr is given below. 

```{r}
# Extracting importance and sorting 
variables.sorted <- varImp(rf.model, scale = FALSE)$importance %>% dplyr::arrange(desc(Overall))
head(variables.sorted)

# Extracting 200 most important genes
genes.important <- rownames(variables.sorted)[1:200]

#Function for modifying gene names for Enrichr
modify.names <- function(gene.vector){
  names <- c()
  gene.list <- strsplit(gene.vector, "[.]")
  for (i in 1:length(gene.vector)){
    names[i] <- unlist(gene.list[i])[1]
  }
  return(names)
}

# writing to txt-file 
write.table(modify.names(genes.important), file = "important_genes.txt", sep = "\t", row.names = FALSE, col.names = FALSE, quote = FALSE)
```
![Figure 4: Descartes Cell Types and Tissue 2021](Descartes_Cell_Types_and_Tissue_2021_bar_graph (1).png)


![Figure 5: KEGG 2021 Human Ontology results from Enrichr](KEGG_2021_Human_bar_graph.png)

# Discussion

Overall, we see that the two machine learning methods classify the 5 tumor types accurately. The prediction accuracy on the testing set is high (above 90%) for almost all classes. The samples that are misclassified the most often in $k$NN and random forest are rectum and stomach samples. This is not surprising, since both tumor types affect the gastrointestinal tract. In random forest, there is only one tumor type that is classified, which is pancreas samples. Despite high overall accuracy of both methods, there are some limitations to this study. 

First we see in the PCA plot (figure 1) that the first two PCs only explain 10% and 8% of the total variance in expression across sample-columns. Although we selected tissues that overlap in the space spanned by PCs, we see that the two PCs do not explain the majority of variance in sample-columns. This could mean that we have selected tumor types that in the feature space are not too difficult to separate. Additionally, the tissue dispersion may have been more pronounced if both axis ranges were scaled to different values, allowing for adjusted dispersion within the plot.


Another limitation is the narrow range of values that is explored when hyper parameter tuning. Due to the size of the data set and limited computational resources, we had to limit the range of the grid search. Furthermore, due to the size of the data set and limitations of computational power, the grid searches covered a narrow set of values. 
uters performing the analysis, the applied hyper parameters were limited to a narrow. This may have resulted in selected parameters being approximately near, but not the exactly at their optimal values. Depending on the computational power of available hardware, the analysis may be repeated to ensure optimal fine tuning of these hyperparameters.

Results from analysis by Enrichr show that the top 200 genes are classified primarily to tissues not included in the data set consisting of pancreas, lung, breast, rectum and stomach. The obtained results are for tissue in the eye, liver, placenta, spleen and kidney. Worth noting is that the 6th result from Enrichr is in fact for pancreatic cells, but none of the other selected tissue appear amongst the most significant results. This is an indication of how the predicted most important genes, as evaluated by random forest, are altered presumably due to the presence of cancer, which alters the expression profiles of cells and tissues. Including a wider range of genes, or all of them, may have produced more accurate tissue predictions, this is however not certain as the data set is based upon cancer data and which may be reflected in enrichment analysis through apparent inaccurate classifications.

However, counter to the results for tissue and cell types, most of the results for KEGG 2021 Human pathways associated with these selected genes appear to indicate or relate to pancreatic disorders, most prominently diabetes. Notably, transcriptional misregulation in cancer, without specified tissue or organ, is the fourth result. Further research could uncover whether there is any correlation between the two disorders, possibly with either indicating the other or just a perchance co-occurrence in our analysis.


# References
1. Yersal, Ozlem, and Sabri Barutca. “Biological subtypes of breast cancer: Prognostic and therapeutic implications.” World journal of clinical oncology vol. 5,3 (2014): 412-24. doi:10.5306/wjco.v5.i3.412

2. Cooper GM. The Cell: A Molecular Approach. 2nd edition. Sunderland (MA): Sinauer Associates; 2000. The Development and Causes of Cancer. Available from: https://www.ncbi.nlm.nih.gov/books/NBK9963/

3. Bray, Freddie et al. “The ever-increasing importance of cancer as a leading cause of premature death worldwide.” Cancer vol. 127,16 (2021): 3029-3030. doi:10.1002/cncr.33587

4. Petrucelli N, Daly MB, Pal T. BRCA1- and BRCA2-Associated Hereditary Breast and Ovarian Cancer. 1998 Sep 4 [Updated 2022 May 26]. In: Adam MP, Everman DB, Mirzaa GM, et al., editors. GeneReviews® [Internet]. Seattle (WA): University of Washington, Seattle; 1993-2022. Available from: https://www.ncbi.nlm.nih.gov/books/NBK1247/

5. Slavin, Thomas Paul et al. “Genetics of gastric cancer: what do we know about the genetic risks?.” Translational gastroenterology and hepatology vol. 4 55. 29 Jul. 2019, doi:10.21037/tgh.2019.07.02

6. Read, Jazlyn et al. “Melanoma genetics.” Journal of medical genetics vol. 53,1 (2016): 1-14. doi:10.1136/jmedgenet-2015-103150

7. Heo, Yong Jin et al. “Integrative Multi-Omics Approaches in Cancer Research: From Biological Networks to Clinical Subtypes.” Molecules and cells vol. 44,7 (2021): 433-443. doi:10.14348/molcells.2021.0042

8. Menyhárt, Otília, and Balázs Győrffy. “Multi-omics approaches in cancer research with applications in tumor subtyping, prognosis, and diagnosis.” Computational and structural biotechnology journal vol. 19 949-960. 22 Jan. 2021, doi:10.1016/j.csbj.2021.01.009

9. Cancer Genome Atlas Research Network et al. “The Cancer Genome Atlas Pan-Cancer analysis project.” Nature genetics vol. 45,10 (2013): 1113-20. doi:10.1038/ng.2764

10. Love, Michael I et al. “Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2.” Genome biology vol. 15,12 (2014): 550. doi:10.1186/s13059-014-0550-8

11. Jolliffe, Ian T, and Jorge Cadima. “Principal component analysis: a review and recent developments.” Philosophical transactions. Series A, Mathematical, physical, and engineering sciences vol. 374,2065 (2016): 20150202. doi:10.1098/rsta.2015.0202

12. Kuhn, Max. "Building predictive models in R using the caret package." Journal of statistical software 28 (2008): 1-26.

13. Kuhn, Max. The caret Package (2019). Last updated: 27.03.2019. Acessed 01.11.2022. Available at: https://topepo.github.io/caret/index.html

14. Zhang, Zhongheng. “Introduction to machine learning: k-nearest neighbors.” Annals of translational medicine vol. 4,11 (2016): 218. doi:10.21037/atm.2016.03.37

15. Raschka, S., Mirjalili, V.Python Machine Learning: Machine Learning and Deep Learning with Python, Scikit-learn, and TensorFlow 2. 2nd edition. Packt Publishing; 2019.

16. Chen, Xi, and Hemant Ishwaran. “Random forests for genomic data analysis.” Genomics vol. 99,6 (2012): 323-9. doi:10.1016/j.ygeno.2012.04.003

17. Wenric, Stephane, and Ruhollah Shemirani. “Using Supervised Learning Methods for Gene Selection in RNA-Seq Case-Control Studies.” Frontiers in genetics vol. 9 297. 3 Aug. 2018, doi:10.3389/fgene.2018.00297