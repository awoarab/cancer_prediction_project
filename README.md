# 

# Description:

Following project was done in collaboration with other students for an introductory class in functional genomics. The project uses RNA-Seq data from the The Cancer Genome Atlas (TCGA) project. The goal of this project is two-fold:
1. Use machine learning methods to classify different tumor types based on transcriptomics data.
2. Extract the most important features (genes) for classification, and perform functional enrichment analysis

This repository is structured in the following directories:
1. Data directory: Contains RNA-Seq count table, as well as txt-file with the most important genes for classification
2. Code directory: Contains an R-script with all the code needed for plotting + data analysis. Contains also final drafts of report (both html-format and Rmarkdown format).
